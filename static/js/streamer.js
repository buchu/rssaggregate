$(document).ready(function(){
    _.templateSettings = {
        interpolate: /\{\-(.+?)\-\}/g
    }

    var addArticle = function (data) {
        console.log(data)
        var compiled = _.template($('#element_template').html())
        console.log(compiled(data))
        $('#list').append(compiled(data))
    }

    var removeArticle = function (data) {
        $('#article_'+data.pk).remove();
    }

    var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
    var ws_path = ws_scheme + '://' + window.location.host + window.location.pathname + "stream/";
    console.log("Connected to "+ ws_path);
    var socket = new ReconnectingWebSocket(ws_path);

    socket.onmessage = function(message) {
        var data = JSON.parse(message.data);

        switch(data.payload.action) {
            case 'create':
                addArticle(data.payload)
                break;
            case 'delete':
                removeArticle(data.payload)
                break;
        }
    }
})