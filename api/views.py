from django.views.generic import ListView

from api.models import Article, Stream


class StreamListView(ListView):
    model = Article
    ordering = '-created_at'

    def get_context_data(self, **kwargs):
        context = super(StreamListView, self).get_context_data(**kwargs)
        context['streams'] = Stream.objects.all()

        return context