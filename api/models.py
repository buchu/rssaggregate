import json

from channels import Channel
from channels.binding.websockets import WebsocketBinding
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify


class Stream(models.Model):
    API = 1
    RSS = 2
    CHANNEL_TYPES = (
        (API, 'Api'),
        (RSS, 'Rss')
    )
    _type = models.IntegerField(choices=CHANNEL_TYPES, null=False)
    name = models.CharField(max_length=256, null=False)
    acronym = models.CharField(max_length=4)
    url = models.URLField(null=False)
    color = models.CharField(max_length=16)

    items_container = models.CharField(max_length=128, help_text='Parent node with all entries')
    item_tag = models.CharField(max_length=128, help_text='Single article element')


class StreamTag(models.Model):
    URL = 'url'
    TITLE = 'title'
    BRIEF = 'brief'
    TYPES = (
        (URL, 'Url'),
        (TITLE, 'Title'),
        (BRIEF, 'Brief')
    )
    stream = models.ForeignKey('Stream', related_name='tags')
    _type = models.CharField(choices=TYPES, null=False, max_length=16)
    tag = models.CharField(max_length=128)
    textable = models.BooleanField(default=True)
    attribute = models.CharField(max_length=64, null=True, blank=True)
    content = models.CharField(max_length=1024, null=True, blank=True)


class Article(models.Model):
    title = models.CharField(max_length=512, null=False)
    slug = models.SlugField(unique=True)
    stream = models.ForeignKey('Stream', related_name='articles')
    created_at = models.DateTimeField(auto_now_add=True)
    url = models.URLField()
    brief = models.TextField(null=True, blank=True)

    @property
    def color(self):
        return self.stream.color

    @property
    def acronym(self):
        return self.stream.acronym


@receiver(pre_save, sender=Article)
def slugify_article(sender, instance, **kwargs):
    if not instance.id and not instance.slug:
        instance.slug = slugify(instance.title)


class ArticleBinding(WebsocketBinding):
    model = Article
    stream = 'articles'
    fields = ['channel', 'title', 'brief', 'url', 'color', 'acronym']

    @classmethod
    def group_names(self, instance, action):
        return ['articles.values']

    def has_permission(self, user, action, pk):
        return True

    def serialize_data(self, instance):
        return {
            'title': instance.title,
            'brief': instance.brief,
            'url': instance.url,
            'stream': {
                'color': instance.stream.color,
                'acronym': instance.stream.acronym
            }
        }
