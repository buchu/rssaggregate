from channels.generic.websockets import WebsocketDemultiplexer


class Demultiplexer(WebsocketDemultiplexer):
    mapping = {
        "articles": 'binding.articles'
    }

    def connection_groups(self):
        return ['articles.values']
