from django.contrib import admin

from api.models import Stream, Article, StreamTag


class ArticleSite(admin.ModelAdmin):
    list_display = ('title',)


class StreamTagInline(admin.TabularInline):
    model = StreamTag


class StreamSite(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [
        StreamTagInline
    ]


admin.site.register(Stream, StreamSite)
admin.site.register(Article, ArticleSite)
