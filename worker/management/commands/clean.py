import datetime

from django.conf import settings
from django.utils import timezone
from django.core.management import BaseCommand

from api.models import Article


class Command(BaseCommand):
    def handle(self, *args, **options):
        Article.objects.filter(
            created_at__lte=timezone.now() - datetime.timedelta(seconds=settings.ARTICLE_EXPIRATION_TIME)
        ).delete()
