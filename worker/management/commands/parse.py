from django.core.management import BaseCommand

from api.models import Stream, Article
from worker.tasks import parse_stream


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        for stream in Stream.objects.all():
            parse_stream(stream.id)
