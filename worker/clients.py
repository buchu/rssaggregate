import requests
import xml.etree.ElementTree as ET

from abc import ABC, abstractmethod
from fake_useragent import UserAgent

from api.models import Stream, Article


class BaseClient(ABC):
    url = None

    def __init__(self, stream_id):
        self.object = Stream.objects.get(id=stream_id)

    @abstractmethod
    def _get_raw_articles(self):
        pass

    @abstractmethod
    def get_articles(self):
        pass

    def _get(self):
        return requests.get(self.object.url, headers={'User-Agent': UserAgent().random}).content


class RssClient(BaseClient):
    def _get_raw_articles(self):
        body = self._get()
        root = ET.fromstring(body)
        if root.find(self.object.items_container):
            root = root.find(self.object.items_container)

        for item in root.iter(self.object.item_tag):
            yield self._build_element(item)

    def _build_element(self, item):
        element = {}
        for tag in self.object.tags.all():
            if tag.textable:
                content = item.find(tag.tag).text
            else:
                content = item.find(tag.tag).attrib[tag.attribute]
            element[tag._type] = content

        return element

    def get_articles(self):
        added = 0
        for raw in self._get_raw_articles():
            print(raw)
            obj, created = Article.objects.get_or_create(stream=self.object, **raw)
            if created:
                added += 1
        return added


class ApiClient(BaseClient):
    def _get_raw_articles(self):
        pass

    def get_articles(self):
        pass
