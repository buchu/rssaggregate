from api.models import Stream
from worker.clients import RssClient, ApiClient

CLIENTS = {
    1: ApiClient,
    2: RssClient
}


def get_client(_type, _id):
    return CLIENTS[_type](_id)


def parse_stream(channel_id):
    stream = Stream.objects.get(id=channel_id)
    print('--------')
    print('updating {}'.format(stream.name))
    client = get_client(stream._type, stream.id)
    num = client.get_articles()
    print('fetched {} new articles'.format(num))

    return
