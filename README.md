# RSSFeeder

A simply Django app to aggregate rss channels into one dashboard. It uses django-channels to communicate with backend via websockets.

## Installation

Install virtualenv as usual and install all requirements

```bash
pip install -r requirements.txt
```

## Usage

For dev purposes you can just

```bash
./manage.py runserver
```

and under http://localhost:8000 you will get dashboard.

To fetch some messages from channels you need to create super user

```bash
./manage.py createsuperuser
```

and add some Stream via admin panel (http://localhost:8000/admin)

## Cron

It uses websockets to communicate model changes onto frontend layer.
To refresh your streams with new data just add this to your cron

```
*/15 * * * * source <path>/to/env/bin && python <path>/to/manage.py parse
```

To clean articles older than 24 hours use clean job

```
* */12 * * * source <path>/to/env/bin && python <path>/to/manage.py clean
```

Expiration time is adjustable in settings.
 

## Development

It is simply django apps which introduce channels idea to make django a real time web framework.

## Copyright

Copyright (c) 2016 Pawel Buchowski.
