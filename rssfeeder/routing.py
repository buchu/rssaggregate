from channels import route, route_class

from api.consumers import Demultiplexer
from api.models import ArticleBinding

channel_routing = [
    route_class(Demultiplexer),
    route('binding.articles', ArticleBinding.consumer)
]